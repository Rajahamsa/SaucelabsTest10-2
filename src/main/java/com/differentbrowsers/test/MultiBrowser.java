package com.differentbrowsers.test;

import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;

import org.testng.annotations.BeforeClass;

import org.testng.annotations.Parameters;

import org.testng.annotations.Test;

public class MultiBrowser {

	public WebDriver driver;

	@Parameters("browser")

	@BeforeClass

	public void beforeTest(String browser) throws Exception {
		
		final String USERNAME = "Rajahamsa";
		  final String ACCESS_KEY = "756b4cfe-b3be-4c9b-9456-516af6af8c02";
		  final String URL = "https://" + USERNAME + ":" + ACCESS_KEY + "@ondemand.saucelabs.com:443/wd/hub";

		if (browser.equalsIgnoreCase("chrome")) {

			 DesiredCapabilities caps = DesiredCapabilities.chrome();
			    caps.setCapability("platform", "Windows 10");
			    caps.setCapability("version", "60.0");
			 
			   driver = new RemoteWebDriver(new URL(URL), caps);
			

		} else if (browser.equalsIgnoreCase("ie")) {

			 DesiredCapabilities caps = DesiredCapabilities.internetExplorer();
			    caps.setCapability("platform", "Windows 10");
			    caps.setCapability("version", "11.0");
			 
			   driver = new RemoteWebDriver(new URL(URL), caps);
			

		}
		
		else if (browser.equalsIgnoreCase("firefox")) {

			 DesiredCapabilities caps = DesiredCapabilities.firefox();
			    caps.setCapability("platform", "Windows 10");
			    caps.setCapability("version", "54.0");
			 
			   driver = new RemoteWebDriver(new URL(URL), caps);
			

		}

	}

	@Test
	public void test() {

		driver.get("http://www.google.com");
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

	}

	@Test
	public void testFacebook() {

		driver.get("http://www.facebook.com");
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

	}

	@AfterClass
	public void afterTest() {

		driver.quit();

	}

}