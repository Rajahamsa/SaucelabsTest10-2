package com.testparallel.methods;

import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ParallelMethods {

	protected WebDriver driver;

	protected WebDriver getDriverInstance() throws Exception {
		final String USERNAME = "Rajahamsa";
		  final String ACCESS_KEY = "756b4cfe-b3be-4c9b-9456-516af6af8c02";
		  final String URL = "https://" + USERNAME + ":" + ACCESS_KEY + "@ondemand.saucelabs.com:443/wd/hub";
		  DesiredCapabilities caps = DesiredCapabilities.chrome();
		    caps.setCapability("platform", "Windows 10");
		    caps.setCapability("version", "60.0");
		 
		   driver = new RemoteWebDriver(new URL(URL), caps);
		return driver;
	}

	@BeforeMethod
	public void setUp() throws Exception {
		driver = getDriverInstance();

	}


	@Test
	public void navigatetoAmazonHomepage() {
		driver.get("https://www.amazon.com/");

	}

	@Test
	public void navigatetoGoogleHomepage() {
		driver.get("https://www.google.com/");

	}

	@Test
	public void navigatetoFacebookHomepage() {
		driver.get("https://www.facebook.com/");

	}

	@AfterMethod
	protected void close() {
		driver.manage().timeouts().implicitlyWait(200,TimeUnit.SECONDS);
		driver.close();
		closebrowser();
	}
	
	@AfterClass
	protected void closebrowser() {
		driver.quit();
	}
	
	

}
